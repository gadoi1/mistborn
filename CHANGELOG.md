# Mistborn Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project strives to adhere to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.2.3] - 2024-04-03

### Added
- Automated translation compilation

### Fixed
- Update status check

### Updated
- Pihole version 2024.03.2

### Removed
- Unused python modules

## [v2.2.1] - 2024-03-24

### Added
- First translations for German, French, Portuguese, Spanish, Dutch, Russian, Ukrainian, Arabic, Farsi, Japanese, Korean.
- Added update status check

### Fixed
- Update and restart processes
- Updated JavaScript dependencies

## [v2.2.0] - 2024-03-21

### Added
- Groups and Group Membership Management from WireGuard menu
- Single Sign On (OAuth2) Provider

### Fixed
- Updated JavaScript dependencies

### Removed
- Gateways removed from WireGuard management

## [v2.1.1] - 2024-03-14

### Fixed
- Sync server WireGuard confs with MFA clients (backward compatibility for Mistborn)

## [v2.1.0] - 2024-03-13

### Added
- WireGuard now uses the same listening port for new clients. Putting Mistborn behind a router with port forwarding becomes trivial.
- `mistborn-cli getconf` will now also show the qrencoded conf file.
- Translations have begun for German, Portuguese, French, Spanish.

### Fixed
- Handling sessions in a browser previously connected with a different Mistborn profile.

